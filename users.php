<?php

use APP\connect\DBconnection;

require_once('class.php');
$requestType = $_SERVER['REQUEST_METHOD'];
$user = new DBconnection();
//Switch statement
switch ($requestType) {
    case 'GET':
        $id = $_GET['id'];
        $found_user = $user->get_user($id);
        if (!$found_user)
            $e = new Exception('Oops.. we cant find that id!', 404);
        if ($e) {
            var_dump($e->getMessage());
            http_response_code($e->getCode());
            throw $e;
        }
        break;
    case 'POST':
        //username
        $username = $_POST['username'];
        if (!$username)
            $h = new Exception('username is required', 400);
        //password
        $password = $_POST['password'];
        if (!$password)
            $h = new Exception('password is required', 400);
        //firstname
        $firstname = $_POST['firstname'];
        if (!$firstname)
            $h = new Exception('firstname is required', 400);
        //lastname
        $lastname = $_POST['lastname'];
        if (!$lastname)
            $h = new Exception('lastname is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        } else {
            //created test
            $sql = "SELECT * FROM users WHERE username = '$username';";
            $result = mysqli_query($user->getconnection(), $sql);;
            var_dump($result);
            if (mysqli_num_rows($result) > 0) {
                echo "username is already in use";
                break;
            } else {
                $post_user = $user->post_user($username, $password, $firstname, $lastname);
                if ($post_user == "yes") {
                    echo "user created successfully";
                }
            }
        }
        break;
    case 'PUT':
        $put = file_get_contents("php://input");
        $put = json_decode($put, true);
        $id = $put['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        //check user exist
        $sql = "SELECT * FROM users WHERE id = $id;";
        $result = mysqli_query($user->getconnection(), $sql);;
        if (mysqli_num_rows($result) > 0) {
            echo "ops we cant find that id";
        }
        $username = $put['username'];
        $password = $put['password'];
        $firstname = $put['firstname'];
        $lastname = $put['lastname'];
        $update = $user->update_user($id, $username, $password, $firstname, $lastname);
        if ($update == "yes") {
            echo "service updated successfully";
        }
        break;
    case 'DELETE':
        $id = $_GET['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        $delete = $user->delete_user($id);
        if ($delete == "yes") {
            echo "service deleted successfully";
        }
        break;
    default:
        break;
}
?>