<?php

namespace APP\connect;
//connect to database
use http\Env\Request;
use PDO;
use PDOException;


class DBconnection
{
    private $host = "localhost";
    private $db_name = "prj1";
    private $db_username = "root";
    private $db_password = "sinaghr7";
    protected $myPDO;

    function __construct()
    {
        try {
            $this->myPDO = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->db_username, $this->db_password);
            $this->myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getconnection()
    {
        return $this->myPDO;
    }

    public function get_user($id)
    {
        if ($id == null) {
            $sql = "SELECT * FROM users";
        } else {
            $sql = "SELECT * FROM users WHERE id = :id ";
        }
        $result = $this->myPDO->prepare($sql);
        $result->execute(['id' => $id]);
        $user = $result->fetch();
        var_dump($user);
        return $user;
    }

    public function get_service($id)
    {
        if ($id == null) {
            $sql = "SELECT * FROM services";
        } else {
            $sql = "SELECT * FROM services WHERE id = ':id' ";
        }
        $result = $this->myPDO->prepare($sql);
        $result->execute(['id' => $id]);
        $service = $result->fetch();
        var_dump($service);
        return $service;
    }

    public function get_userService($id)
    {
        if ($id == null) {
            $sql = "SELECT * FROM userservices
            INNER JOIN users
            ON userservices.user_id = users.id
            INNER JOIN services
            ON userservices.service_id = services.id";
        } else {
            $sql = "SELECT * FROM userservices
            INNER JOIN users
            ON userservices.user_id = users.id
            INNER JOIN services
            ON userservices.service_id = services.id
            WHERE id = :id ";
        }
        $result = $this->myPDO->prepare($sql);
        $result->execute(['id' => $id]);
        $userService = $result->fetch(PDO::FETCH_ASSOC);
        var_dump($userService);
        return $userService;
    }

    public function post_user($username, $password, $firstname, $lastname)
    {
        $query = "INSERT INTO users (username , password , firstname , lastname ) VALUES (:username , :password , :firstname , :lastname)";
        $error = $this->myPDO->prepare($query);
        $error->execute(['username' => $username, 'password' => $password, 'firstname' => $firstname, 'lastname' => $lastname]);
        if ($error) {
            return "yes";
        }
    }

    public function post_service($title, $description, $orders, $price)
    {
        $query = "INSERT INTO services (title , description , orders , price ) VALUES (:title , :description , :orders , :price)";
        $result = $this->myPDO->prepare($query);
        $error = $result->execute(['title' => $title, 'description' => $description, 'orders' => $orders, 'price' => $price]);
        if ($error) {
            return "yes";
        }
    }
    public function post_userService($user_id, $service_id, $status, $start, $finish)
    {
        $query = "INSERT INTO userservices (user_id , service_id , status , start , finish ) VALUES (:user_id , :service_id , :status , :start , :finish)";
        $error = $this->myPDO->prepare($query);
        $error->execute(['user_id' => $user_id, 'service_id' => $service_id, 'status' => $status, 'start' => $start, 'finish' => $finish]);
        if ($error) {
            return "yes";
        }
    }

    public function update_user($id, $username, $password, $firstname, $lastname)
    {
        $query = "UPDATE users SET username = :username , password = :password , firstname = :firstname , lastname = :lastname WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['id' => $id, 'username' => $username, 'password' => $password, 'firstname' => $firstname, 'lastname' => $lastname]);
        if ($test) {
            return "yes";
        }
    }

    public function update_service($id, $title, $description, $orders, $price)
    {
        $query = "UPDATE services SET title = :title ,description = :description , orders = :orders , price = :price WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['id' => $id, 'title' => $title, 'description' => $description, 'orders' => $orders, 'price' => $price]);
        if ($test) {
            return "yes";
        }
    }

    public function update_userService($user_id, $service_id, $status, $start, $finish)
    {
        $query = "UPDATE userservices SET user_id = :user_id , service_id = :service_id , status = :status , start = :start WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['user_id' => $user_id, 'service_id' => $service_id, 'status' => $status, 'start' => $start, 'finish' => $finish]);
        if ($test) {
            return "yes";
        }
    }

    public function delete_user($id)
    {
        $query = "DELETE FROM users WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['id' => $id]);
        if ($test) {
            return "yes";
        }
    }

    public function delete_service($id)
    {
        $query = "DELETE FROM services WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['id' => $id]);
        if ($test) {
            return "yes";
        }
    }
    public function delete_userService($id)
    {
        $query = "DELETE FROM userservices WHERE id = :id";
        $result = $this->myPDO->prepare($query);
        $test = $result->execute(['id' => $id]);
        if ($test) {
            return "yes";
        }
    }
}

?>