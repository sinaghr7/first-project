<?php
use APP\connect\DBconnection;

require_once('class.php');
$requestType = $_SERVER['REQUEST_METHOD'];
$service = new DBconnection();
//Switch statement
switch ($requestType) {
    case 'GET':
        $id = $_GET['id'];
        $found_service = $service->get_service($id);
        if (!$found_service)
            $e = new Exception('Oops.. we cant find that id!', 404);
        if ($e) {
            var_dump($e->getMessage());
            http_response_code($e->getCode());
            throw $e;
        }
        break;
    case 'POST':
        $title = $_POST['title'];
        $description = $_POST['description'];
        $orders = $_POST['orders'];
        $price = $_POST['price'];
        $service->post_service($title, $description, $orders, $price);
        break;
    case 'PUT':
        $put_service = file_get_contents("php://input");
        $put_service = json_decode($put_service, true);
        $id = $put_service['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        $title = $put_service['title'];
        $description = $put_service['description'];
        $orders = $put_service['orders'];
        $price = $put_service['price'];
        $update = $service->update_service($id, $title, $description, $orders, $price);
        if($update == "yes"){
        echo "service updated successfully";
    }
        break;
    case 'DELETE':
        $id = $_GET['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        $delete = $service->delete_service($id);
        if($delete == "yes"){
            echo "service deleted successfully";
        }
        break;
    default:
        break;
}
?>