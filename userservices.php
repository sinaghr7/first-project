<?php

use APP\connect\DBconnection;

require_once('class.php');
$requestType = $_SERVER['REQUEST_METHOD'];
$userService = new DBconnection();
//Switch statement
switch ($requestType) {
    case 'GET':
        $id = $_GET['id'];
        $found_userService = $userService->get_userService($id);
        if (!$found_userService)
            $e = new Exception('Oops.. we cant find that id!', 404);
        if ($e) {
            var_dump($e->getMessage());
            http_response_code($e->getCode());
            throw $e;
        }
        break;
    case 'POST':
        //user_id
        $user_id = $_POST['user_id'];
        if (!$user_id)
            $h = new Exception('user_id is required', 400);
        //service_id
        $service_id = $_POST['service_id'];
        if (!$service_id)
            $h = new Exception('service_id is required', 400);
        //status
        $status = $_POST['status'];
        if (!$status)
            $h = new Exception('status is required', 400);
        //start
        $start = $_POST['start'];
        if (!$start)
            $h = new Exception('start is required', 400);
        //finish
        $finish = $_POST['finish'];
        if (!$finish)
            $h = new Exception('finish is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        } else {
            $post_userService = $userService->post_userService($user_id, $service_id, $status, $start ,$finish);
             if ($post_userService == "yes") {
                echo "userService created successfully";
            }
        }
        break;
    case 'PUT':
        $put = file_get_contents("php://input");
        $put = json_decode($put, true);
        $id = $put['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        //check userService exist
        $sql = "SELECT * FROM userservices WHERE id = $id;";
        $result = mysqli_query($userService->getconnection(), $sql);;
        if (mysqli_num_rows($result) > 0) {
            echo "ops we cant find that id";
        }
        $user_id = $put['user_id'];
        $service_id = $put['service_id'];
        $status = $put['status'];
        $start = $put['start'];
        $finish = $put['finish'];
        $update = $userService->update_userService($user_id , $service_id , $status , $start , $finish);
        if ($update == "yes") {
            echo "userService updated successfully";
        }
        break;
    case 'DELETE':
        $id = $_GET['id'];
        if (!$id)
            $h = new Exception('id is required', 400);
        if ($h) {
            var_dump($h->getMessage());
            http_response_code($h->getCode());
            throw $h;
        }
        $delete = $userService->delete_userService($id);
        if ($delete == "yes") {
            echo "userService deleted successfully";
        }
        break;
    default:
        break;
}
?>